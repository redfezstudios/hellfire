#!/usr/bin/env python3

import socket, sys
from datetime import datetime, date, time

debugBox = None
W_PORT = 50002
PORT = 50003
ADDR = 'localhost'
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

def setDebugBox(dB):
    global debugBox
    debugBox = dB

def setAddr(adr, p):
    PORT = p
    ADDR = adr

def log(msg):
    global debugBox
    try:
        if debugBox:
            debugBox.texto += (str(msg) + "\n")
    except Exception as err:
        pass

    s.sendto(str(msg).encode("utf8", "backslashreplace"), (ADDR, PORT))

if __name__ == "__main__":
    if len( sys.argv ) > 2:
        PORT = sys.argv[1]

    adr = ('', PORT)
    s.bind(adr)

    print("\n Debug console connected (localhost:" + str(W_PORT) + ")\n------------------------------------------------\n")
    while True:
        try:
            com = input("$ > ");
            if(com == "!close"):
                break

            s.sendto(str(com).encode("utf8", "backslashreplace"), (ADDR, W_PORT));

            comando, address = s.recvfrom(1024); #RECV
            data, address = s.recvfrom(1024); #Numero de lineas
            if(comando.decode("utf8", "backslashreplace") != "close"):
                i = int(data);

                while i > 0:
                    data, address = s.recvfrom(1024)
                    if(data and i > 1):
                        print(data.decode("utf8", "backslashreplace"))
                    i-=1;
            else:
                data, address = s.recvfrom(1024)

        except Exception as e:
            print("[{}] {} > {}".format(type(e).__name__, str(datetime.now()).split(".")[0], e))

    s.close()
