#include <RosquilleraReforged/rf_engine.h>
#include <RosquilleraReforged/rf_debugconsole.h>
#include "comandos.h"

#include <iostream>
#include <string>
using namespace std;

#include "base/configuration.h"
#include "base/mainprocess.h"

extern string startingScene;

int main(int argc, const char* argv[])
{
	if(argc > 1 && (Configuration::getConfig("debug")) == "44164524q")
	{
		if(strcmp(argv[1], "--help") == 0 || strcmp(argv[1], "-h") == 0)
		{
			cout << "Modo de uso: ./Hellfire [OPCIONES]" << endl;
			cout << "Ejecuta el juego" << endl << endl;
			cout << "Opciones:" << endl;
			cout << "  -h, --help	muestra esta ayuda" << endl;
			cout << "  -s, --scene	inicia en la escena indicada" << endl;
			cout << "Por ejemplo:" << endl;
			cout << "	./Hellfire --scene Mainmenu" << endl << endl;
			cout << "Si tienes algún problema envía un email a <tuzmakel@gmail.com>" << endl << endl;
			exit(0);
		}

		for(int i = 1; i < argc; i++)
		{
			if(strcmp(argv[i], "--scene") == 0 || strcmp(argv[i], "-s") == 0)
			{
				startingScene = argv[++i];
			}
		}
	}

	generaComandos();
	//RF_SocketDebugger::port = stoi(Configuration::getConfig("debug_port"));
	RF_Engine::Start<MainProcess>((Configuration::getConfig("debug")) == "44164524q");
	return 0;
}


#include <RosquilleraReforged/windows_wrapper.h>
