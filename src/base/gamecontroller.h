#ifndef GAMECONTROLLER_H
#define GAMECONTROLLER_H

#include <RosquilleraReforged/rf_process.h>
#include <functional>
#include <string>
#include <unordered_map>

struct KeyEvent
{
  KeyEvent(std::string id_, std::string axis_, std::function<void()> callback_):
    id{id_}, axis{axis_}, callback{callback_} {}
  KeyEvent(){}
  ~KeyEvent() = default;

  std::string id {};
  std::string axis {};
  std::function<void()> callback;
  bool flanco {false};
};

class GameController : public RF_Process
{
public:
    static GameController *instance;

    GameController():RF_Process("GameController"){}
    virtual ~GameController(){}

    virtual void Start();
    virtual void Update();

    static void AddEvent(KeyEvent&& keyEvent);
    static void RemoveEvent(std::string keyEvent);

  private:
    std::unordered_map<std::string, KeyEvent> keyEvents;
};

#endif //GAMECONTROLLER_H
