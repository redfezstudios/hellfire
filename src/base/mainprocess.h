#ifndef MAINPROCESS_H
#define MAINPROCESS_H

#include <RosquilleraReforged/rf_process.h>

class MainProcess : public RF_Process
{
	public:
		MainProcess():RF_Process("MainProcess"){}
		virtual ~MainProcess(){}
		void Start();

	private:
		bool fscrn;
};

#endif //MAINPROCESS_H
