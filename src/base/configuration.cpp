#include "base/configuration.h"
#include <DoughEngine/inputmanager.h>

#include <sys/stat.h>
#include <dirent.h>
#include <string>
#include <iostream>
#include <fstream>
using namespace std;

Configuration* Configuration::instance = nullptr;
void Configuration::checkInstance()
{
  if(instance == nullptr)
  {
    new Configuration();
  }
}

Configuration::Configuration()
{
  instance = this;
  parser = new Parser("options.json");
}

Configuration::~Configuration()
{
  instance = nullptr;
}

string Configuration::getConfig(string field)
{
  checkInstance();

  string val;
  Parser::json_string(instance->parser->jsvalue["config"][field], val);
  return val;
}

void Configuration::setConfig(string field, string value)
{
  checkInstance();

  instance->parser->jsvalue["config"][field] = value;
  ofstream salida("options.json");
  Json::StyledStreamWriter writer;
  writer.write(salida, instance->parser->jsvalue);
}

vector<string> Configuration::saveList()
{
  vector<string> files;
  DIR *dir = opendir("saves/");
  if(dir == nullptr)
  {
    RF_Engine::Debug("Save system [Error]: No se puede abrir el directorio");
    return files;
  }

  struct dirent *ent;
  string name;

  while((ent = readdir(dir)) != nullptr)
  {
    if((strcmp(ent->d_name, ".")!=0) && (strcmp(ent->d_name, "..")!=0))
    {
      name = ent->d_name;
      const size_t pos = name.find_last_of('.');
      name.erase(pos);
      files.push_back(name);
    }
  }

  return files;
}

void Configuration::setSaveFile(string file)
{
  checkInstance();
  instance->saveFile = file;
  RF_Engine::Debug("Asignado fichero de guardado: " + file);
}

void Configuration::saveGame()
{
  checkInstance();

  struct stat buffer;
  if(stat(("saves/" + instance->saveFile + ".json").c_str(), &buffer) != 0)
  {
    ofstream s("saves/" + instance->saveFile + ".json");
    s << "{}";
    s.close();
  }

  Parser *saveParser = new Parser("saves/" + instance->saveFile + ".json");
  for(auto m = instance->gameData.begin(); m != instance->gameData.end(); m++)
  {
    saveParser->jsvalue["gameData"][m->first] = m->second;
  }

  ofstream salida("saves/" + instance->saveFile + ".json");
  Json::StyledStreamWriter writer;
  writer.write(salida, saveParser->jsvalue);

  delete saveParser;
}

void Configuration::loadGame()
{
  checkInstance();

  struct stat buffer;
  if(stat(("saves/" + instance->saveFile + ".json").c_str(), &buffer) != 0)
  {
    RF_Engine::Debug("Load system [Error]: El archivo de guardado indicado no existe");
    return;
  }

  Parser *loadParser = new Parser("saves/" + instance->saveFile + ".json");
  instance->gameData.clear();
  for(auto m : loadParser->jsvalue["gameData"].getMemberNames())
  {
    Parser::json_string(loadParser->jsvalue["gameData"][m], instance->gameData[m]);
  }

  delete loadParser;
}

string Configuration::getGameData(string key)
{
  checkInstance();
  return (instance->gameData.find(key) != instance->gameData.end()) ? instance->gameData[key] : "";
}

void Configuration::setGameData(string key, string val)
{
  checkInstance();
  instance->gameData[key] = val;
}

string Configuration::getController()
{
  return Configuration::getConfig("inputController");
}

void Configuration::setController(string controller)
{
  Configuration::setConfig("inputController", controller);
  InputManager::clearAxis();

  InputManager::setAxis("exeControl",
    KeyGroup::Make({&RF_Input::key[_esc], &RF_Input::key[_close_window], &RF_Input::jkey[_select]}),
    KeyGroup::Make({})
  );
  InputManager::setAxis("Horizontal",
    KeyGroup::Make({&RF_Input::key[_d], &RF_Input::key[_right], &RF_Input::jkey[_jpad_dir_right]}),
    KeyGroup::Make({&RF_Input::key[_a], &RF_Input::key[_left], &RF_Input::jkey[_jpad_dir_left]})
  );
  InputManager::setAxis("Vertical",
    KeyGroup::Make({&RF_Input::key[_s], &RF_Input::key[_down], &RF_Input::jkey[_jpad_dir_down]}),
    KeyGroup::Make({&RF_Input::key[_w], &RF_Input::key[_up], &RF_Input::jkey[_jpad_dir_up]})
  );
  InputManager::setAxis("Selection",
    KeyGroup::Make({&RF_Input::key[_return], &RF_Input::key[_space], &RF_Input::jkey[_circle]}),
    KeyGroup::Make({})
  );
  InputManager::setAxis("Fire",
    KeyGroup::Make({&RF_Input::key[_j], &RF_Input::jkey[_equis]}),
    KeyGroup::Make({})
  );
  InputManager::setAxis("ChangeWeapon",
    KeyGroup::Make({&RF_Input::key[_l], &RF_Input::jkey[_circle]}),
    KeyGroup::Make({})
  );
}

bool& Configuration::EscPressed()
{
  Configuration::checkInstance();
  return Configuration::instance->escPressed;
}

RF_Structs::Vector2<float>& Configuration::PlayerPosition()
{
    Configuration::checkInstance();
    return *Configuration::instance->playerPosition;
}

RF_Structs::Vector2<float>& Configuration::MapPosition()
{
    Configuration::checkInstance();
    return Configuration::instance->mapPosition;
}
