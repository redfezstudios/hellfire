#include "base/mainprocess.h"

#include <RosquilleraReforged/rf_engine.h>
#include <RosquilleraReforged/rf_input.h>
#include <DoughEngine/camera.h>
#include <DoughEngine/scenemanager.h>
#include <DoughEngine/utils/locale.h>

#include "base/configuration.h"
#include "base/gamecontroller.h"

#include "scenes/splash.h"
#include "scenes/mainmenu.h"
#include "scenes/game.h"

#include <dirent.h>
using namespace std;

string startingScene = "SplashScreen";

void MainProcess::Start()
{
  //AXIS
  if(Configuration::getController() != "")
  {
    Configuration::setController(Configuration::getController());

    if(Configuration::getController() == "gamepad" && RF_Input::gGameController == nullptr)
    {
        Configuration::setController("keyboard");
    }
  }
  else
  {
    if(RF_Input::gGameController == nullptr)
    {
      Configuration::setController("keyboard");
    }
    else
    {
      Configuration::setController("gamepad");
    }
  }

  //CREAMOS LA VENTANA
  fscrn = (Configuration::getConfig("fullscreen") == "true");
  window = RF_Engine::addWindow("Hellfire", 1280, 720, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, (!fscrn) ? SDL_WINDOW_OPENGL : SDL_WINDOW_OPENGL|SDL_WINDOW_FULLSCREEN);
  RF_Engine::MainWindow(window);

  //ASIGNAMOS EL COLOR A LA VENTANA
  window->setBackColor(255, 255, 255);

  //REGISTRAMOS ESCENAS
  SceneManager::RegisterScene<Splash>();
  SceneManager::RegisterScene<Mainmenu>();
  SceneManager::RegisterScene<Game>();

  //CREAMOS CÁMARA
  //RF_Engine::newTask<Camera>(id);

  //CREAMOS GAMECONTROLLER
  RF_Engine::newTask<GameController>(id);

  //CREAMOS EL EXECONTROL
  GameController::AddEvent(KeyEvent("ExeControl", "exeControl", [](){RF_Engine::Status() = false;}));

  SceneManager::Start(startingScene);
}
