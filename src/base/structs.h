#ifndef STRUCTS_H
#define STRUCTS_H

#include <RosquilleraReforged/rf_structs.h>
using namespace RF_Structs;

typedef struct
{
  int opt;
  string key;
  Vector2<float> position;
  Vector2<float> sombra;
  Vector2<float> cursorOffset;
} Option;

#endif //STRUCTS_H
