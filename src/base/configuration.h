#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include <RosquilleraReforged/rf_engine.h>
#include <RosquilleraReforged/utils/parser.h>

#include <map>
#include <vector>
using namespace std;
using namespace RF_Structs;

class Configuration
{
  public:
    static Configuration *instance;
    virtual ~Configuration();

    static string getConfig(string field);
    static void setConfig(string field, string value);
    static RF_Window* MainWindow() {return RF_Engine::MainWindow();}

    static vector<string> saveList();
    static void setSaveFile(string file);
    static void saveGame();
    static void loadGame();

    static string getGameData(string key);
    static void setGameData(string key, string val);
    map<string, string> gameData;

    static void setController(string controller);
    static string getController();

    string playingGamescene;
    bool escPressed = false;
    static bool& EscPressed();

    RF_Structs::Vector2<float>* playerPosition;
    static RF_Structs::Vector2<float>& PlayerPosition();

    RF_Structs::Vector2<float> mapPosition;
    static RF_Structs::Vector2<float>& MapPosition();

  private:
    static void checkInstance();

    Configuration();

    Parser *parser;
    string saveFile = "";
};

#endif //CONFIGURATION_H
