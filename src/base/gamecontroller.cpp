#include "base/gamecontroller.h"
#include <DoughEngine/inputmanager.h>

GameController* GameController::instance = nullptr;

void GameController::Start()
{
  if(instance == nullptr) instance = this;
  else signal = RF_Structs::S_KILL;
}

void GameController::Update()
{
  for(auto& item : keyEvents)
  {
    if(InputManager::getAxis(item.second.axis) != 0.0)
    {
      if(!item.second.flanco)
      {
        item.second.callback();
        item.second.flanco = true;
      }
    }
    else
    {
      item.second.flanco = false;
    }
  }
}

void GameController::AddEvent(KeyEvent&& keyEvent)
{
  if(instance->keyEvents.find(keyEvent.id) == instance->keyEvents.end())
  {
    instance->keyEvents[keyEvent.id] = std::move(keyEvent);
  }
}

void GameController::RemoveEvent(std::string keyEvent)
{
  if(instance->keyEvents.find(keyEvent) != instance->keyEvents.end())
  {
    instance->keyEvents.erase(keyEvent);
  }

}
