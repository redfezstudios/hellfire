#ifndef PLAYER_H
#define PLAYER_H

#include <RosquilleraReforged/rf_process.h>
#include <DoughEngine/animator.h>
#include <functional>

#include "shooting/shootinfo.h"

class Player : public RF_Process
{
  public:
    Player():RF_Process("Player"){}
    virtual ~Player(){}

    virtual void Start();
    virtual void Update();
    virtual void Draw();
    virtual void Dispose();

  private:
    Animator* animator;
    RF_Structs::Vector2<float> speed = {500.0, 500.0};

    int shootStrategyIndex {0};
    bool shootFlanco[2] = {false, false};
    function<void()> shootStrategy[4];

    ShootingStrategy::ShooterInfo shootInfo;
};

#endif //BACKGROUND_H
