#include "gameobjects/ingame/player.h"

#include <RosquilleraReforged/rf_assetmanager.h>
#include <DoughEngine/inputmanager.h>

#include "base/configuration.h"
#include "base/gamecontroller.h"
#include "shooting/shootingstrategy.h"

void Player::Start()
{
  animator = new Animator(&graph);

  RF_AssetManager::loadAssetPackage(Configuration::getConfig("resPath") + "player");
  animator->Add("idle", "player", "nave", 1, 1.0, true);
  animator->tryToExecute("idle");

  transform.position.x = 1280*0.5;
  transform.position.y = 720*0.5;

  Configuration::instance->playerPosition = &transform.position;

  shootInfo.process = this;
  shootInfo.target = "Enemy";
  shootInfo.bulletSpeed = 50.0;

  shootStrategy[0] = [this](){
      this->shootInfo.direction.x = 1;
      this->shootInfo.direction.y = 0;
      ShootingStrategy::ShootStraight(this->shootInfo);
    };

  shootStrategy[1] = [this](){
      this->shootInfo.direction.x = 1;
      this->shootInfo.direction.y = 1;
      ShootingStrategy::ShootStraight(this->shootInfo);
      this->shootInfo.direction.x = -1;
      this->shootInfo.direction.y = 1;
      ShootingStrategy::ShootStraight(this->shootInfo);
      this->shootInfo.direction.x = 1;
      this->shootInfo.direction.y = -1;
      ShootingStrategy::ShootStraight(this->shootInfo);
      this->shootInfo.direction.x = -1;
      this->shootInfo.direction.y = -1;
      ShootingStrategy::ShootStraight(this->shootInfo);

      this->shootInfo.direction.x = 0;
      this->shootInfo.direction.y = 1;
      ShootingStrategy::ShootStraight(this->shootInfo);
      this->shootInfo.direction.x = 1;
      this->shootInfo.direction.y = 0;
      ShootingStrategy::ShootStraight(this->shootInfo);
      this->shootInfo.direction.x = 0;
      this->shootInfo.direction.y = -1;
      ShootingStrategy::ShootStraight(this->shootInfo);
      this->shootInfo.direction.x = -1;
      this->shootInfo.direction.y = 0;
      ShootingStrategy::ShootStraight(this->shootInfo);
    };

  shootStrategy[2] = [this](){
      this->shootInfo.direction.x = 0;
      this->shootInfo.direction.y = 1;
      ShootingStrategy::ShootStraight(this->shootInfo);
    };
  /*shootStrategy[1] = [this](){this->Shoot2();};
  shootStrategy[2] = [this](){this->Shoot3();};
  shootStrategy[3] = [this](){this->Shoot4();};*/

  GameController::AddEvent(KeyEvent("Disparo", "Fire", [this](){this->shootStrategy[this->shootStrategyIndex]();}));
  GameController::AddEvent(KeyEvent("CambioArma", "ChangeWeapon", [this](){this->shootStrategyIndex = (this->shootStrategyIndex < 2) ? this->shootStrategyIndex + 1 : 0;}));

}

void Player::Draw()
{
  animator->Draw();
}

void Player::Dispose()
{
  delete animator;
}

void Player::Update()
{
  transform.position.x += InputManager::getAxis("Horizontal") * speed.x * RF_Engine::instance->Clock.deltaTime;
  transform.position.y += InputManager::getAxis("Vertical")   * speed.y * RF_Engine::instance->Clock.deltaTime;

  if(graph)
  {
    if(transform.position.x < 0 + graph->w*0.5) transform.position.x = 0 + graph->w*0.5;
    else if(transform.position.x > window->width() - graph->w*0.5) transform.position.x = window->width() - graph->w*0.5;

    if(transform.position.y < 0 + graph->h*0.5) transform.position.y = 0 + graph->h*0.5;
    else if(transform.position.y > window->height() - graph->h*0.5) transform.position.y = window->height() - graph->h*0.5;
  }
}
