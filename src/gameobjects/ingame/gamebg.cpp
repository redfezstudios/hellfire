#include "gameobjects/ingame/gamebg.h"

#include <RosquilleraReforged/rf_assetmanager.h>
#include <RosquilleraReforged/rf_primitive.h>
#include <DoughEngine/camera.h>
#include <limits>
#include <algorithm>
#include <thread>

#include "base/configuration.h"

bool ParallaxLayer::compareByDepth(const ParallaxLayer &a, const ParallaxLayer &b)
{
  return a.depth > b.depth;
}

void GameBG::addLayer(string package, string resource, float speed)
{
  layers.push_back(ParallaxLayer(RF_AssetManager::Get<RF_Gfx2D>(package, resource), speed));
  std::sort(layers.begin(), layers.end(), ParallaxLayer::compareByDepth);
}

void GameBG::Update()
{
  Configuration::MapPosition().x += speed.x * RF_Engine::instance->Clock.deltaTime;
  //Configuration::MapPosition().y += speed.y * RF_Engine::instance->Clock.deltaTime;
  Configuration::MapPosition().y = 0.1 * Configuration::PlayerPosition().y;
}

void GameBG::Draw()
{
  std::vector<std::thread> threads;
  unsigned int poolsize = (std::thread::hardware_concurrency()+1)*2;

  for (unsigned int t = 0; t < poolsize; t++) {
    threads.emplace_back([=, this](){
        int capa, i, j, z;
        Uint32 p, pixel;
        Uint8 foo, a;
        Uint8 a_o;

        int chunk = this->graph->h/poolsize;
        j = t * chunk;

        for(z = 0; z < chunk; z++)
        {
          for(i = 0; i < this->graph->w; i++)
          {
            capa = pixel = a_o = 0;

            do
            {
              //Obtenemos el pixel
              p = RF_Primitive::getPixel(
                this->layers[capa].graph,
                (i + (int)(Configuration::MapPosition().x*this->layers[capa].depth))%this->layers[capa].graph->w,
                (j+z + (int)(Configuration::MapPosition().y*this->layers[capa].depth))%this->layers[capa].graph->h
              );

              //Lo desmembramos
              SDL_GetRGBA(p, this->layers[capa].graph->format, &foo, &foo, &foo, &a);

              //Sumamos colores
              pixel |= p;
              a_o |= a;

              //Iteramos capa
              capa++;
            } while (a_o < 0xFF && capa < this->layers.size());
            RF_Primitive::putPixel(this->graph, i, j+z, pixel);
          }
        }
      });
  }

  for (auto& thread : threads) {
    thread.join();
  }
}
