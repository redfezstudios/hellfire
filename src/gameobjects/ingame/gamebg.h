#ifndef BACKGROUND_H
#define BACKGROUND_H

#include <RosquilleraReforged/rf_layer.h>
using namespace RF_Structs;

struct ParallaxLayer
{
  static bool compareByDepth(const ParallaxLayer &a, const ParallaxLayer &b);


  SDL_Surface* graph = nullptr;
  float depth = 0.0;
};

class GameBG : public RF_Layer
{
  public:
    GameBG():RF_Layer("Background"){}
    virtual ~GameBG(){}

    virtual void Update();
    virtual void Draw();

    void addLayer(string package, string resource, float speed = 0.0);
    RF_Structs::Vector2<float> speed = {50.0, 0.0};

  private:
    vector<ParallaxLayer> layers;
    static RF_Structs::Vector2<float> position;
};

#endif //BACKGROUND_H
