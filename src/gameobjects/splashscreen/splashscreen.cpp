#include "gameobjects/splashscreen/splashscreen.h"

#include <RosquilleraReforged/rf_primitive.h>
#include <RosquilleraReforged/rf_assetmanager.h>
#include <DoughEngine/scenemanager.h>

#include "base/configuration.h"
#include "base/mainprocess.h"

void SplashScreen::Start()
{
    RF_AssetManager::loadAssetPackage(Configuration::getConfig("resPath") + "splashscreen");

    max_logos = 2;
    bgImg.push_back(RF_AssetManager::Get<RF_Gfx2D>("splashscreen", "banner"));
    bgImg.push_back(RF_AssetManager::Get<RF_Gfx2D>("splashscreen", "logo"));

    RF_Layer::Start();
    RF_Primitive::clearSurface(graph, 0xFFFFFF);
    transform.position.x = RF_Engine::MainWindow()->width() >> 1;
    transform.position.y = RF_Engine::MainWindow()->height() >> 1;

    step = 0.0;
}

void SplashScreen::Update()
{
    step += RF_Engine::instance->Clock.deltaTime*20;
    stp = (step < 50.0) ? step : ((step > 100.0) ? step - 50.0 : 50.0);

    RF_Primitive::clearSurface(graph, 0xFFFFFF);

    for(i = 0; i < bgImg[logo]->w; i+=3)
    {
      for(j = 0; j < bgImg[logo]->h; j+=3)
      {
        xx = i - ((rand()%40) - 20) * (50-stp);
        yy = j - ((rand()%40) - 20) * (50-stp);

        if(0 <= xx && 0 <= yy && bgImg[logo]->w > xx && bgImg[logo]->h > yy)
        {
          RF_Primitive::putPixel(graph, xx, yy, RF_Primitive::getPixel(bgImg[logo],i,j));
          RF_Primitive::putPixel(graph, xx, yy+1, RF_Primitive::getPixel(bgImg[logo],i,j+1));
          RF_Primitive::putPixel(graph, xx, yy+2, RF_Primitive::getPixel(bgImg[logo],i,j+2));

          RF_Primitive::putPixel(graph, xx+1, yy, RF_Primitive::getPixel(bgImg[logo],i+1,j));
          RF_Primitive::putPixel(graph, xx+1, yy+1, RF_Primitive::getPixel(bgImg[logo],i+1,j+1));
          RF_Primitive::putPixel(graph, xx+1, yy+2, RF_Primitive::getPixel(bgImg[logo],i+1,j+2));

          RF_Primitive::putPixel(graph, xx+2, yy, RF_Primitive::getPixel(bgImg[logo],i+2,j));
          RF_Primitive::putPixel(graph, xx+2, yy+1, RF_Primitive::getPixel(bgImg[logo],i+2,j+1));
          RF_Primitive::putPixel(graph, xx+2, yy+2, RF_Primitive::getPixel(bgImg[logo],i+2,j+2));
        }
      }
    }

    if(step > 120.0)
    {
      step = 0.0;
      logo++;
      if(logo >= max_logos)
      {
        SceneManager::Change("Mainmenu");
      }
    }
}
