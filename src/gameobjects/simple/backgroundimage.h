#ifndef BACKGROUNDIMAGE_H
#define BACKGROUNDIMAGE_H

#include <RosquilleraReforged/rf_process.h>
using namespace RF_Structs;

class BackgroundImage : public RF_Process
{
  public:
    BackgroundImage():RF_Process("BackgroundImage"){}
    virtual ~BackgroundImage(){}

    void setImg(string package, string resource);
};

#endif //BACKGROUNDIMAGE_H
