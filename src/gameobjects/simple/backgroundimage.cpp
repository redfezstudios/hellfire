#include "gameobjects/simple/backgroundimage.h"

#include <RosquilleraReforged/rf_assetmanager.h>

#include <limits>
using namespace std;

void BackgroundImage::setImg(string package, string resource)
{
  graph = RF_AssetManager::Get<RF_Gfx2D>(package, resource);
}
