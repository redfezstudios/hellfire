#ifndef LABEL_H
#define LABEL_H

#include <RosquilleraReforged/rf_engine.h>
#include <RosquilleraReforged/rf_textmanager.h>
#include <DoughEngine/utils/locale.h>
using namespace RF_Structs;

class Label : public RF_Process
{
  enum {
    _ALIGN_CENTER,
    _ALIGN_LEFT = 1,
    _ALIGN_RIGHT = 2
  };

  public:
    Label():RF_Process("Label"){}
    virtual ~Label();

    void Escribe();
    void Reposiciona();
    void Configure(string k, Vector2<float> p, Vector2<float> s, int a = 0, SDL_Color c = {255,255,255}, SDL_Color shadow = {0,0,0});
    void SetPosition(Vector2<float> p);

    void setVisibility(bool v);
    bool getVisibility();

    bool autoUpdate = true;

  protected:
    int i;
    int w;
    TTF_Font *font = nullptr, *tmpfont = nullptr;
    string lang;
    string t = "", s = "";

    string key;
    Vector2<float> position;
    Vector2<float> sombra;
    int align = 0;
    SDL_Color color = {255,255,255};
    SDL_Color shadow = {0,0,0};

    bool _v;
    bool visible = true;
};

#endif //LABEL_H
