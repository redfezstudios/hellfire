#include "gameobjects/simple/label.h"

Label::~Label()
{
  if(s != ""){RF_TextManager::DeleteText(s);}
  if(t != ""){RF_TextManager::DeleteText(t);}
}

void Label::Escribe()
{
  i = 0;
  if(s != ""){RF_TextManager::DeleteText(s);}
  if(t != ""){RF_TextManager::DeleteText(t);}

  if(!visible)
  {
    s = "";
    t = "";
    return;
  }

  if(font == nullptr){font = RF_TextManager::Font;}

  tmpfont = RF_TextManager::Font;
  RF_TextManager::Font = font;

  s = RF_TextManager::Write(key, shadow, position + sombra, id);
  RF_Engine::getTask(s)->zLayer--;
  t = RF_TextManager::Write(key, color, position, id);

  RF_TextManager::Font = tmpfont;
}

void Label::Reposiciona()
{
  if(s == "" || t == "") return;

  i = 0;
  w = 0;
  if(align > _ALIGN_CENTER)
  {
    w = RF_Engine::getTask(s)->graph->w >> 1;
    if(align == _ALIGN_RIGHT)
    {
      w*=-1;
    }
  }
  RF_Engine::getTask(t)->transform.position = {position.x + (float)w, position.y};
  RF_Engine::getTask(s)->transform.position = {position.x + sombra.x + (float)w, position.y};

}

void Label::Configure(string k, Vector2<float> p, Vector2<float> s, int a, SDL_Color c, SDL_Color shadow)
{
  key = k;
  position = p;
  sombra = s;
  align = a;
  color = c;

  Escribe();
  Reposiciona();
}

void Label::SetPosition(Vector2<float> p)
{
  position = p;
}

void Label::setVisibility(bool v)
{
  _v = visible;
  visible = v;
  if(visible != _v)
  {
    Escribe();
    Reposiciona();
  }

}

bool Label::getVisibility()
{
  return visible;
}
