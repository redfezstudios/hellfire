#ifndef CURSOR_H
#define CURSOR_H

#include <RosquilleraReforged/rf_process.h>
#include "base/structs.h"
using namespace RF_Structs;

class Cursor : public RF_Process
{
  public:
    Cursor():RF_Process("Cursor"){}
    virtual ~Cursor(){}

    void setOptionList(Option *optl, unsigned int mopt);
    void Start();
    void Update();

    virtual void Selection(unsigned int opc){}

  private:
    Option *optionList;
    int activeoption = 0;
    int maxOption = 0;
    bool pulsado = true;
};

#endif //CURSOR_H
