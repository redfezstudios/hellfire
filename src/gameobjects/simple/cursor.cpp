#include "gameobjects/simple/cursor.h"
#include <RosquilleraReforged/rf_assetmanager.h>
#include <DoughEngine/inputmanager.h>

void Cursor::setOptionList(Option *optl, unsigned int mopt)
{
  optionList = optl;
  maxOption = mopt;
}

void Cursor::Start()
{
  graph = RF_AssetManager::Get<RF_Gfx2D>("gui", "cursor");
  zLayer = 1000;
}

void Cursor::Update()
{
  if(maxOption > 0)
  {
    transform.position = optionList[activeoption].position + optionList[activeoption].cursorOffset;

    if(InputManager::getAxis("Vertical") > 0)
    {
      if(!pulsado)
      {
        pulsado = true;
        activeoption = (++activeoption < maxOption) ? activeoption : 0;
      }
    }
    else if(InputManager::getAxis("Vertical") < 0)
    {
        if(!pulsado)
        {
          pulsado = true;
          activeoption = (--activeoption >= 0) ? activeoption : maxOption-1;
        }
    }
    else if(InputManager::getAxis("Selection") != 0)
    {
      if(!pulsado)
      {
        pulsado = true;
        Selection(optionList[activeoption].opt);
      }
    }
    else
    {
      pulsado = false;
    }
  }
}
