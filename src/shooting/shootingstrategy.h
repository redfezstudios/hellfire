#ifndef SHOOTINGSTRATEGY_H
#define SHOOTINGSTRATEGY_H

#include "shooting/shootinfo.h"
#include <string>

namespace ShootingStrategy
{
  void ShootStraight(ShooterInfo& info);
};

#endif //SHOOTINGSTRATEGY_H
