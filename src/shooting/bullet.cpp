#include "shooting/bullet.h"
#include <RosquilleraReforged/rf_assetmanager.h>

#include "base/configuration.h"

void ShootingStrategy::Bullet::Start()
{
  RF_AssetManager::loadAssetPackage(Configuration::getConfig("resPath") + "bullets");
  graph = RF_AssetManager::Get<RF_Gfx2D>("bullets", "simple");
}

void ShootingStrategy::Bullet::Update()
{
  transform.position.x += move.x * speed * RF_Engine::instance->Clock.deltaTime;
  transform.position.y += move.y * speed * RF_Engine::instance->Clock.deltaTime;

  if(transform.position.x < 0 ||
     transform.position.x > window->width() ||
     transform.position.y < 0 ||
     transform.position.y > window->height()) signal = RF_Structs::S_KILL;
}

void ShootingStrategy::Bullet::SetPosition(float x, float y)
{
  transform.position.x = x;
  transform.position.y = y;
}

void ShootingStrategy::Bullet::SetPosition(RF_Structs::Vector2<float> position)
{
  SetPosition(position.x, position.y);
}

void ShootingStrategy::Bullet::SetDirection(float x, float y)
{
  move.x = x;
  move.y = y;

  // Calculamos el arcoseno (en radianes), lo pasamos a grados y lo ponemos en negativo si hace falta
  transform.rotation = acos(move.x/sqrt(move.x*move.x + move.y*move.y))*57.2963885456 * ((move.y < 0.0) ? -1 : 1);

  move.x = speed * cos(transform.rotation/57.2963885456);
  move.y = speed * sin(transform.rotation/57.2963885456);

  RF_Engine::Debug(to_string(move.x) + ", " + to_string(move.y));
}

void ShootingStrategy::Bullet::SetDirection(RF_Structs::Vector2<float> direction)
{
  SetDirection(direction.x, direction.y);
}

void ShootingStrategy::Bullet::SetTarget(std::string target_)
{
  target = target_;
}

void ShootingStrategy::Bullet::SetSpeed(float speed_)
{
  speed = speed_;
}
