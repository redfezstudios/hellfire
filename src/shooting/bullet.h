#ifndef BULLET_H
#define BULLET_H

#include <RosquilleraReforged/rf_structs.h>
#include <RosquilleraReforged/rf_process.h>
#include <string>

namespace ShootingStrategy
{
  class Bullet : public RF_Process
  {
    public:
      Bullet():RF_Process("Bullet"){}
      virtual ~Bullet(){}

      virtual void Start();
      virtual void Update();

      void SetPosition(float x, float y);
      void SetPosition(RF_Structs::Vector2<float> position);

      void SetDirection(float x, float y);
      void SetDirection(RF_Structs::Vector2<float> direction);

      void SetTarget(std::string target_);

      void SetSpeed(float speed_);

    private:
      RF_Structs::Vector2<float> move;
      std::string target {};

      float speed = 20.0;//2000.0;
      float tmpSpeed[2];
  };
};

#endif //BULLET_H
