#include "shooting/shootingstrategy.h"

#include <RosquilleraReforged/rf_engine.h>

#include "shooting/bullet.h"

void ShootingStrategy::ShootStraight(ShooterInfo& info)
{
  Bullet* bullet = RF_Engine::getTask<Bullet>(RF_Engine::newTask<Bullet>(info.process->id));
  bullet->SetTarget(info.target);

  RF_Structs::Vector2<float> position;
  position.x = info.process->transform.position.x + (info.process->graph->w*0.5 + bullet->graph->w)*((info.direction.x < 0.0) ? -1.0 : ((info.direction.x > 0.0) ? 1.0 : 0.0));
  position.y = info.process->transform.position.y + (info.process->graph->h*0.5 + bullet->graph->h)*((info.direction.y < 0.0) ? -1.0 : ((info.direction.y > 0.0) ? 1.0 : 0.0));
  bullet->SetPosition(position);

  bullet->SetDirection(info.direction);

  if(info.bulletSpeed > 0.0)
  {
    bullet->SetSpeed(info.bulletSpeed);
  }
}
