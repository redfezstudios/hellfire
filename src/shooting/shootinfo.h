#ifndef SHOOTINFO_H
#define SHOOTINFO_H

#include <RosquilleraReforged/rf_process.h>
#include <string>

namespace ShootingStrategy
{
  struct ShooterInfo
  {
    RF_Process* process {nullptr};
    std::string target {};
    RF_Structs::Vector2<float> direction;
    float bulletSpeed {0.0};
  };
};

#endif //SHOOTINFO_H
