#ifndef GAME_H
#define GAME_H
#include <DoughEngine/scene.h>

#include "base/structs.h"
#include "gameobjects/ingame/gamebg.h"
#include "gameobjects/ingame/player.h"

class Game : public Scene
{
  public:
    Game():Scene("Game"){}
    virtual ~Game(){}

    virtual void SceneUpdate();
    virtual void Configure();

  private:
    GameBG* background = nullptr;
    Player* player = nullptr;
};

#endif //GAME_H
