#include "scenes/game.h"

#include <RosquilleraReforged/rf_engine.h>
#include <RosquilleraReforged/rf_assetmanager.h>
#include <RosquilleraReforged/rf_soundmanager.h>
#include <DoughEngine/inputmanager.h>
#include <DoughEngine/scenemanager.h>

#include "base/configuration.h"

void Game::SceneUpdate()
{
  
}

void Game::Configure()
{
  RF_AssetManager::loadAssetPackage(Configuration::getConfig("resPath") + "game");
  background = AddElement<GameBG>();
  background->addLayer("game", "bg1", 1.0);
  background->addLayer("game", "bg2", 2.0);

  player = AddElement<Player>();


}
