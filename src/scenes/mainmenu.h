#ifndef MAINMENU_H
#define MAINMENU_H
#include <DoughEngine/scene.h>

#include "base/structs.h"
#include "gameobjects/simple/label.h"
#include "gameobjects/simple/cursor.h"
#include "gameobjects/ingame/gamebg.h"

class MainmenuCursor : public Cursor
{
  public:
    MainmenuCursor():Cursor(){}
    virtual ~MainmenuCursor(){}

    void Selection(unsigned int opc);
};

class Mainmenu : public Scene
{
  public:
    Mainmenu():Scene("Mainmenu"){}
    virtual ~Mainmenu(){}

    enum {
      _START,
      _EXIT,
      _FOO_OPT
    };

    Option optionList[_FOO_OPT];

    virtual void Configure();

  private:
    GameBG* background = nullptr;
    Label *l;
    int i;
    MainmenuCursor *c;
};

#endif //MAINMENU_H
