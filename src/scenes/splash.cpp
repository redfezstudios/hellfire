#include "splash.h"
#include <DoughEngine/inputmanager.h>
#include "base/configuration.h"
#include "gameobjects/splashscreen/splashscreen.h"

void Splash::Configure()
{
    AddElement<SplashScreen>();
}
