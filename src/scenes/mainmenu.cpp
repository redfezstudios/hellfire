#include "scenes/mainmenu.h"

#include <RosquilleraReforged/rf_engine.h>
#include <RosquilleraReforged/rf_assetmanager.h>
#include <RosquilleraReforged/rf_textmanager.h>
#include <RosquilleraReforged/rf_soundmanager.h>
#include <DoughEngine/inputmanager.h>
#include <DoughEngine/scenemanager.h>
#include "base/configuration.h"

void MainmenuCursor::Selection(unsigned int opc)
{
  switch(opc)
  {
    case 0:
      if(Configuration::getConfig("savefile") == ""){Configuration::setConfig("savefile", to_string(rand()));}
      Configuration::setSaveFile(Configuration::getConfig("savefile"));
      Configuration::loadGame();

      if(Configuration::getGameData("startingScene") == ""){Configuration::setGameData("startingScene", Configuration::getConfig("startingScene"));}
      Configuration::instance->playingGamescene = Configuration::getGameData("startingScene");
      SceneManager::Start("Game");
      //SceneManager::Start(Configuration::getGameData("startingScene"));
      break;
    case 1:
      RF_Engine::Status() = false;
      break;
  }
}

void Mainmenu::Configure()
{
  RF_AssetManager::loadAssetPackage(Configuration::getConfig("resPath") + "gui");
  RF_SoundManager::changeMusic("gui", "mainmenu");

  optionList[_START] = {_START, "Start", {570, 544}, {2.0, 2.0}, {50.0,10.0}};
  optionList[_EXIT] = {_EXIT, "Exit", {570, 594}, {2.0, 2.0}, {50.0,10.0}};

  RF_TextManager::Font = RF_AssetManager::Get<RF_Font>("gui", "Debby");

  RF_AssetManager::loadAssetPackage(Configuration::getConfig("resPath") + "game");
  background = AddElement<GameBG>();
  background->addLayer("game", "bg1", 1.0);
  background->addLayer("game", "bg2", 2.0);

  transform.position.x = 1280.0*0.5;
  transform.position.y = 720.0*0.5;
  Configuration::instance->playerPosition = &transform.position;

  for(i = 0; i < _FOO_OPT; i++)
  {
    l = AddElement<Label>();
    l->Configure(optionList[i].key, optionList[i].position, optionList[i].sombra, 2);
  }

  c = AddElement<MainmenuCursor>();
  c->setOptionList(optionList, _FOO_OPT);
}
