#ifndef SPLASH_H
#define SPLASH_H

#include <DoughEngine/scene.h>

class Splash : public Scene
{
  public:
    Splash():Scene("SplashScreen"){}
    virtual ~Splash(){}

    void Configure();
};

#endif //SPLASH_H
