#ifndef COMANDOS_H
#define COMANDOS_H

#include <RosquilleraReforged/rf_debugconsole.h>
#include <RosquilleraReforged/rf_engine.h>
#include <RosquilleraReforged/rf_taskmanager.h>
#include <RosquilleraReforged/rf_assetmanager.h>
#include "base/configuration.h"

#include <vector>
using namespace std;

void cierra(int argc, const char *argv[])
{
	RF_Engine::Status() = false;
}

void taskList(int argc, const char *argv[])
{
	for(RF_Process *p : RF_TaskManager::instance->getTaskList())
	{
		if(p->type != "RF_Text")
		{
			RF_DebugConsoleListener::writeLine(p->id + " -> " + p->type);
		}
	}
}

void getConfig(int argc, const char *argv[])
{
	RF_DebugConsoleListener::writeLine(Configuration::getConfig(string(argv[0])));
}

void loadPackage(int argc, const char *argv[])
{
	if(!RF_AssetManager::isLoaded(string(argv[0])))
	{
		RF_AssetManager::loadAssetPackage(Configuration::getConfig("resPath") + string(argv[0]));
		RF_DebugConsoleListener::writeLine("Paquete de recursos cargado");
	}
	else
	{
		RF_DebugConsoleListener::writeLine("El paquete de recursos ya esta cargado");
	}
}

void unloadPackage(int argc, const char *argv[])
{
	if(RF_AssetManager::isLoaded(string(argv[0])))
	{
		RF_AssetManager::unloadAssetPackage(Configuration::getConfig("resPath") + string(argv[0]));
		RF_DebugConsoleListener::writeLine("Paquete de recursos descargado");
	}
	else
	{
		RF_DebugConsoleListener::writeLine("El paquete de recursos no esta cargado");
	}
}

void sendSignal(int argc, const char* argv[])
{
	string pid = string(argv[0]);
	if(!RF_Engine::existsTask(pid))
	{
		RF_DebugConsoleListener::writeLine("No existe ningun proceso con ese ID");
		return;
	}

	RF_Engine::sendSignal(pid, atoi(argv[1]));
}

void saveList(int argc, const char *argv[])
{
	vector<string> files = Configuration::saveList();
	for(int i = 0; i < files.size(); i++)
	{
		RF_DebugConsoleListener::writeLine(files[i]);
	}
}
void setSaveFile(int argc, const char *argv[])
{
	Configuration::setSaveFile(string(argv[0]));
	RF_DebugConsoleListener::writeLine("Asignado fichero de guardado: " + string(argv[0]));
}
void save(int argc, const char *argv[])
{
	Configuration::saveGame();
}

void viewGameData(int argc, const char *argv[])
{
	if(Configuration::instance == nullptr)
	{
		RF_DebugConsoleListener::writeLine("No hay ninguna entidad que mostrar");
		return;
	}

	for(auto m = Configuration::instance->gameData.begin(); m != Configuration::instance->gameData.end(); m++)
	{
		RF_DebugConsoleListener::writeLine(m->first + " => " + m->second);
	}
}

void generaComandos()
{
	RF_DebugConsoleListener::addCommand("close", new RF_DebugCommand("cierra el juego", 0, &cierra));
	RF_DebugConsoleListener::addCommand("taskList", new RF_DebugCommand("lista los procesos", 0, &taskList));
	RF_DebugConsoleListener::addCommand("getConfig", new RF_DebugCommand("muestra la variable de configuración solicitada", 1, &getConfig));
	RF_DebugConsoleListener::addCommand("loadPackage", new RF_DebugCommand("carga un paquete de recursos", 1, &loadPackage));
	RF_DebugConsoleListener::addCommand("unloadPackage", new RF_DebugCommand("descarga un paquete de recursos", 1, &unloadPackage));
	RF_DebugConsoleListener::addCommand("sendSignal", new RF_DebugCommand("envía una señal al proceso indicado", 2, &sendSignal));
	RF_DebugConsoleListener::addCommand("saveList", new RF_DebugCommand("lista todos los ficheros de guardado", 0, &saveList));
	RF_DebugConsoleListener::addCommand("setSaveFile", new RF_DebugCommand("asigna como fichero de guardado el seleccionado", 1, &setSaveFile));
	RF_DebugConsoleListener::addCommand("save", new RF_DebugCommand("guarda el juego", 0, &save));
	RF_DebugConsoleListener::addCommand("viewGameData", new RF_DebugCommand("muestra el contenido de Configuration::GameData", 0, &viewGameData));
}
#endif //COMANDOS_H
